package com.example.springbootrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.springbootrestapi.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	Produto findById(long id);
	
}
